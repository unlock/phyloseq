package nl.munlock;

import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.rdfhdt.hdt.hdt.HDT;
import org.rdfhdt.hdt.hdt.HDTManager;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;

public class Phyloseq {
    public static final Logger logger = Logger.getLogger(Phyloseq.class);
    public static PrintWriter logFileWriter;
    public static PrintWriter metadataFileWriter;
    public static Domain domain;
    public static PrintWriter asvFileWriter;
    public static PrintWriter taxonFileWriter;
    public static PrintWriter seqFileWriter;
    private static HashSet<String> metadataProcessed = new HashSet<>();
    private static String metadataPrefix;

    public static void generate(File hdtFile) throws Exception {
        // Prepare
        if (domain == null)
            domain = new Domain("");
        if (metadataFileWriter == null)
            metadataFileWriter = new PrintWriter(App.commandOptions.prefix + "_metadata.tsv");
        if (logFileWriter == null)
            logFileWriter = new PrintWriter(App.commandOptions.prefix + ".log");
        if (asvFileWriter == null)
            asvFileWriter = new PrintWriter(App.commandOptions.prefix + "_asv.tsv");
        if (taxonFileWriter == null)
            taxonFileWriter = new PrintWriter(App.commandOptions.prefix + "_tax.tsv");
        if (seqFileWriter == null)
            seqFileWriter = new PrintWriter(App.commandOptions.prefix + "_seq.tsv");


        // logger.info("Processing " + hdtFile);
        HashSet<String> identifiers = new HashSet<>();
        org.rdfhdt.hdt.hdt.HDT hdt;
        try {
            hdt = HDTManager.mapIndexedHDT(hdtFile.getAbsolutePath(), null);
        } catch (IOException e) {
            logger.error("Failed to mount " + hdtFile);
            System.err.println(e.getMessage());
            return;
        }

        String asvType;
        if (App.commandOptions.rejectedASV) {
            asvType = "<http://gbol.life/0.1/RejectedASV> <http://gbol.life/0.1/ASVSet>";
        } else {
            asvType = "<http://gbol.life/0.1/ASVSet>";
        }

        // For each library
        HashMap<String, HashSet<String>> libraries = new HashMap<>();
        try {
            Iterable<ResultLine> resultLines = domain.getRDFSimpleCon().runQuery(hdt, "library.sparql", App.commandOptions.readLength, App.commandOptions.database);
            for (ResultLine resultLine : resultLines) {
                HashSet<String> files = new HashSet<>();
                files.add(resultLine.getLitString("ffile"));
                files.add(resultLine.getLitString("rfile"));
                String library = resultLine.getIRI("library");
                if (libraries.containsKey(library))
                    System.err.println("uhm?");
                libraries.put(library, files);
            }
        } catch (Exception e) {
            logger.error("Retrieving library information failed for " + hdtFile + " " + e.getMessage());
        }

        for (String library : libraries.keySet()) {
            // logger.info("Obtaining information for library " + library + " from " + hdtFile);
            metadataPrefix = "assay_";
            metadataHDTWriter(hdt, logFileWriter, hdtFile, "mAssay.sparql", metadataFileWriter, library);
            metadataPrefix = "sample_";
            metadataHDTWriter(hdt, logFileWriter, hdtFile, "mSample.sparql", metadataFileWriter, library);
            // Using the domain of project metadata
            metadataPrefix = "ou_";
            metadataDomainWriter(App.metadataDomain, logFileWriter, hdtFile, "mObservationUnit.sparql", metadataFileWriter, libraries.get(library));
            metadataPrefix = "study_";
            metadataDomainWriter(App.metadataDomain, logFileWriter, hdtFile, "mStudy.sparql", metadataFileWriter, libraries.get(library));
            metadataPrefix = "inv_";
            metadataDomainWriter(App.metadataDomain, logFileWriter, hdtFile, "mInvestigation.sparql", metadataFileWriter, libraries.get(library));
            metadataPrefix = "project_";
            metadataDomainWriter(App.metadataDomain, logFileWriter, hdtFile, "mProject.sparql", metadataFileWriter, libraries.get(library));

            try {
                Iterable<ResultLine> resultLines = domain.getRDFSimpleCon().runQuery(hdt, "asv.sparql", App.commandOptions.readLength, App.commandOptions.database, asvType, library);
                if (!resultLines.iterator().hasNext()) {
                    logger.error("No asv information found for " + library + " from " + hdtFile);
                    logFileWriter.println("No asv information found for " + library + " from " + hdtFile);
                }
                // ASV abundance per sample
                for (ResultLine resultLine : resultLines) {
                    String sampleName = resultLine.getLitString("sampleName").replace("Sample1_", "").replace("_f.fastq.gz", "");
                    String asvid = resultLine.getLitString("asvid");
                    String count = resultLine.getLitString("readCount");
                    identifiers.add(sampleName);
                    // Row, Column, Count
                    asvFileWriter.println(asvid + "\t" + sampleName + "\t" + count);
                }
            } catch (Exception e) {
                logger.error("Retrieving asv information failed for " + hdtFile +" "+ e.getMessage());
            }

            try {
                Iterable<ResultLine> resultLines = domain.getRDFSimpleCon().runQuery(hdt, "tax.sparql", App.commandOptions.readLength, App.commandOptions.database, asvType, library);
                if (!resultLines.iterator().hasNext()) {
                    logger.error("No taxon information found for " + library + " from " + hdtFile);
                    logFileWriter.println("No taxon information found for " + library + " from " + hdtFile);
                }

                for (ResultLine resultLine : resultLines) {
                    String asvid = resultLine.getLitString("asvid");
                    String taxon = resultLine.getLitString("taxon");
                    // Row... multiple columns
                    taxonFileWriter.println(asvid + "\t" + StringUtils.join(taxon.split(";"), "\t"));
                }
            } catch (Exception e) {
                logger.error("Retrieving tax information failed for " + hdtFile +" "+ e.getMessage());
            }

            try {
                Iterable<ResultLine> resultLines = domain.getRDFSimpleCon().runQuery(hdt, "sequence.sparql", App.commandOptions.readLength, App.commandOptions.database, asvType, library);
                if (!resultLines.iterator().hasNext()) {
                    logger.error("No sequence data found for " + library + " from " + hdtFile);
                    logFileWriter.println("No sequence data found for " + library + " from " + hdtFile);
                }

                for (ResultLine resultLine : resultLines) {
                    String asvid = resultLine.getLitString("asvid");
                    String seq = resultLine.getLitString("seq");
                    String fragment = resultLine.getLitString("fragment");
                    if (fragment == null) {
                        fragment = "refmock";
                    }
                    // Row... multiple columns
                    seqFileWriter.println(asvid + " target_subfragment=" + fragment + "\t" + seq);
                }
            } catch (Exception e) {
                logger.error("Retrieving sequence information failed for " + hdtFile +" "+ e.getMessage());
            }
        }
    }

    private static void metadataDomainWriter(Domain domain, PrintWriter logFileWriter, File hdtFile, String query, PrintWriter metadataFileWriter, HashSet<String> files) {
        if (metadataProcessed.contains(StringUtils.join(files, " "))) {
            // logger.info("Already processed " + StringUtils.join(files, " "));
            return;
        }
        metadataProcessed.add(StringUtils.join(files, " "));

        // logger.info("Obtaining metadata for " + StringUtils.join(files, " "));
        try {
            Iterable<ResultLine> resultLines = domain.getRDFSimpleCon().runQuery(query, true, files.toArray()[0], files.toArray()[1]);
            if (!resultLines.iterator().hasNext()) {
                logger.error("No metadata found for " + query + " " + hdtFile);
                logFileWriter.println("No metadata found for " + query + " " + hdtFile);
            }

            for (ResultLine resultLine : resultLines) {
                String id = resultLine.getLitString("id");
                String predicate = metadataPrefix + resultLine.getIRI("predicate").replaceAll(".*#", "").replaceAll(".*/", "");
                String object = resultLine.getLitString("object");
                String line = id + "\t" + predicate + "\t" + object;

                metadataFileWriter.println(line);
            }
        } catch (Exception e) {
            logger.error("Retrieving metadata information failed for " + hdtFile +" using files " + StringUtils.join(files, " ") + e.getMessage());
        }
    }

    private static void metadataHDTWriter(HDT hdt, PrintWriter logFileWriter, File hdtFile, String query, PrintWriter metadataFileWriter, String library) {
        // logger.info("Obtaining metadata for " + library);
        try {
            Iterable<ResultLine> resultLines = domain.getRDFSimpleCon().runQuery(hdt, query, library);
            if (!resultLines.iterator().hasNext()) {
                logger.error("No metadata found for " + query + " " + hdtFile);
                logFileWriter.println("No metadata found for " + query + " " + hdtFile);
            }

            for (ResultLine resultLine : resultLines) {
                String id = resultLine.getLitString("id");
                String predicate = metadataPrefix + resultLine.getIRI("predicate").replaceAll(".*#", "").replaceAll(".*/", "");
                String object = resultLine.getLitString("object");
                String line = id + "\t" + predicate + "\t" + object;

                metadataFileWriter.println(line);
            }
        } catch (Exception e) {
            logger.error("Retrieving metadata information failed for " + hdtFile +" using files " + library + " " + e.getMessage());
        }
    }
}
