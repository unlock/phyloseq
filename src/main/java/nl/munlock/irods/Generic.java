package nl.munlock.irods;

import org.apache.log4j.Level;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.DataTransferOperations;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.slf4j.LoggerFactory;

import java.io.File;

public class Generic {
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(Generic.class);

    public static void downloadFile(Connection connection, File download) throws JargonException {
        IRODSFile irodsFile = connection.fileFactory.instanceIRODSFile(download.getAbsolutePath());
        if (!irodsFile.exists()) {
            throw new JargonException("File " + irodsFile + " does not exist");
        }
        if (new File("." + download).exists())
            return;

        org.apache.log4j.Logger.getLogger("org.nl.wur.ssb.irods.jargon.core.transfer").setLevel(Level.OFF);
        DataTransferOperations dataTransferOperationsAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount);

        // Create folder directory
        File directory = new File("." + download.getAbsolutePath().replaceAll(download.getName() + "$", ""));
        directory.mkdirs();

        File localFile = new File("." + download);

        while (localFile.exists()) {
            localFile.delete();
        }

        // Disables the logger for the transfer as it easily gives thousands of lines...
        org.apache.log4j.Logger.getLogger("org.nl.wur.ssb.irods.jargon.core.transfer").setLevel(Level.OFF);
        log.info("Downloading " + localFile.getName());

        dataTransferOperationsAO.getOperation(irodsFile, localFile, null, null);
    }
}
