package nl.munlock.irods;

import org.irods.jargon.core.connection.ClientServerNegotiationPolicy;
import org.irods.jargon.core.connection.IRODSAccount;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.IRODSAccessObjectFactory;
import org.irods.jargon.core.pub.IRODSFileSystem;
import org.irods.jargon.core.pub.io.IRODSFileFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.irods.jargon.core.connection.ClientServerNegotiationPolicy.SslNegotiationPolicy.CS_NEG_REFUSE;
import static org.irods.jargon.core.connection.ClientServerNegotiationPolicy.SslNegotiationPolicy.CS_NEG_REQUIRE;

public class Connection {
    public IRODSFileSystem irodsFileSystem;
    public IRODSAccount irodsAccount;
    public IRODSAccessObjectFactory accessObjectFactory;
    public IRODSFileFactory fileFactory;
    private nl.munlock.options.irods.CommandOptions commandOptions;
    private static final Logger log = LoggerFactory.getLogger(Connection.class);

    public Connection(nl.munlock.options.irods.CommandOptions commandOptions) throws JargonException {
        connect(commandOptions);
        this.commandOptions = commandOptions;
    }

    private void connect(nl.munlock.options.irods.CommandOptions commandOptions) throws JargonException {
        // Initialize account object
        irodsAccount = IRODSAccount.instance(commandOptions.host,Integer.parseInt(commandOptions.port),commandOptions.username, commandOptions.password,"",commandOptions.zone,"");

        // set SSL settings
        ClientServerNegotiationPolicy clientServerNegotiationPolicy = new ClientServerNegotiationPolicy();
        ClientServerNegotiationPolicy.SslNegotiationPolicy sslPolicy;
        if (commandOptions.sslPolicyString.matches("CS_NEG_REQUIRE")) {
            sslPolicy = CS_NEG_REQUIRE;
        } else if (commandOptions.sslPolicyString.matches("CS_NEG_REFUSE")) {
            sslPolicy = CS_NEG_REFUSE;
        } else {
            throw new JargonException("SSL policy not recognised: " + commandOptions.sslPolicyString);
        }

        clientServerNegotiationPolicy.setSslNegotiationPolicy(sslPolicy);
        irodsAccount.setClientServerNegotiationPolicy(clientServerNegotiationPolicy);

        irodsFileSystem = IRODSFileSystem.instance();

        accessObjectFactory = irodsFileSystem.getIRODSAccessObjectFactory();

        fileFactory = accessObjectFactory.getIRODSFileFactory(irodsAccount);
    }

    public void close() {
        try {
            this.irodsFileSystem.close();
            this.accessObjectFactory.closeSession();
        } catch (JargonException e) {
            e.printStackTrace();
        }
    }

    public void reconnect() throws JargonException {
        this.close();
        this.connect(this.commandOptions);
    }

    public nl.munlock.options.irods.CommandOptions getCommandOptionsIRODS() {
        return this.commandOptions;
    }
}
