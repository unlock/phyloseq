package nl.munlock;

import nl.munlock.irods.Connection;
import nl.munlock.options.irods.CommandOptions;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

import static nl.munlock.Phyloseq.*;

public class App {
    /**
     * Main landing point for yaml or kubernetes runs...
     *
     * @param args
     * @throws Exception
     */
    private static final Logger log = Generic.getLogger(App.class, false);
    public static CommandOptions commandOptions;
    public static Domain metadataDomain;

    public static void main(String[] args) throws Exception {
        commandOptions = new CommandOptions(args);
        // Make the irods connection
        Connection connection = new Connection(commandOptions);

        // Downloading job file
        nl.munlock.irods.Generic.downloadFile(connection, new File(commandOptions.jobFile));

        Scanner scanner = new Scanner(new File("." + commandOptions.jobFile));
        HashSet<String> hdts = new HashSet<>();
        HashSet<String> turtles = new HashSet<>();
        ArrayList<String> lines = new ArrayList<>();
        // Load file in as array
        while (scanner.hasNextLine()) {
            String file = scanner.nextLine();
            if (file.endsWith(".hdt")) {
                hdts.add(file);
            }
            if (file.endsWith(".ttl")) {
                turtles.add(file);
            }
        }

        scanner.close();

        // Turn all turtle files into a new domain object
        metadataDomain = new Domain("");
        for (String turtle : turtles) {
            // Download
            nl.munlock.irods.Generic.downloadFile(connection, new File(turtle));
            // And validate
            nl.munlock.irods.Generic.downloadFile(connection, new File(turtle));
            log.info("Merging " + turtle);
            Domain domain = new Domain("file://." + turtle);
            metadataDomain.getRDFSimpleCon().getModel().add(domain.getRDFSimpleCon().getModel().listStatements());
            domain.close();
        }

        // Generate objects for phyloseq or biom while downloading them. Redownload if job fails?
        int counter = 0;
        for (String hdt : hdts) {
            // Download
            nl.munlock.irods.Generic.downloadFile(connection, new File(hdt));
            // And validate
            nl.munlock.irods.Generic.downloadFile(connection, new File(hdt));
            // Counter for process printing
            counter = counter + 1;
            if (counter % 100 == 0)
                log.info("Processing " + counter + " " + hdt);
            // Parse the hdt file to create phyloseq content
            Phyloseq.generate(new File("." + hdt));
        }

        // shutting down all writers
        logFileWriter.close();
        metadataFileWriter.close();
        domain.close();
        asvFileWriter.close();
        taxonFileWriter.close();
        seqFileWriter.close();

        // Check the metadata but should be ok now...
        scanner = new Scanner(new File(commandOptions.prefix + "_metadata.tsv"));
        HashMap<String, String> metadata = new HashMap<>();
        HashMap<String, String> headers = new HashMap<>();
        HashSet<String> content = new HashSet<>();
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] lineSplit = line.split("\t");
            String key1 = lineSplit[0];
            String key2 = lineSplit[1];
            String value = lineSplit[2];
            String keys = key1 + "\t" + key2;

            headers.put(keys, value);

            if (metadata.containsKey(keys)) {
                if (!metadata.get(keys).equals(value)) {
                    // Often occurrence when sample dataset is used elsewhere
                    if (!keys.contains("logicalPath")) {
                        log.error("Duplication info found " + line);
                        log.error("Duplication info found " + keys + " " + metadata.get(keys));
                    }
                }
            } else {
                metadata.put(keys, value);
            }
        }
    }
}
