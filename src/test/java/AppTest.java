/*
 * This Java source file was generated by the Gradle 'init' task.
 */

import nl.munlock.App;
import org.junit.Test;

public class AppTest {

    // Performing the job test
    @Test
    public void testPhyloseq() throws Exception {
        String[] args = {
                "-i", "/unlock/home/koehorst/phyloseq/TKI_BO3B.job",
                "-database", "SILVA_138.1_SSURef_tax_silva.fasta.gz",
                "-readLength", "100",
                "-prefix", "TKI_BO3B"
        };
        App.main(args);
    }
}